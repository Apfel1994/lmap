#include "LMapper.h"

#include <cassert>
#include <list>

#include <glm/ext.hpp>
#include <glm/geometric.hpp>

#include <ren/Mesh.h>
#include <ren/Texture.h>

#if defined(USE_SW_RENDER)
    #include <ren/SW/SW.h>
#endif

namespace LMapperInternal {
    inline lm::Vertex MakeVertex(const float *attribs, lm::eLayout layout) {
        if (layout == lm::PxyzNxyzTuv) {
            return{ glm::make_vec3(&attribs[0]), glm::make_vec3(&attribs[3]), glm::make_vec2(&attribs[6]) };
        }
        return{};
    }
}

lm::LMapper::LMapper(int res) : ray_depth_(8), lightmap_res_(res), preview_iteration_(0), samples_done_(0),
                                    preview_pixels_dirty_(false), stage_(PrepareMaps), light_cache_samples_(8), final_gather_samples_(128),
                                    scale_(32.0f) {
    Init();
    // create non-valid first node
    bvh_nodes_.push_back({});
}

lm::LMapper::~LMapper() {
#if defined(USE_SW_RENDER)
    swDeleteFramebuffer(f32_framebuffer_);
    swDeleteFramebuffer(u8_framebuffer_);
    swDeleteProgram(bake_attr_program_);
    swDeleteProgram(bake_albedo_program_);
#endif
}

unsigned lm::LMapper::LoadMesh(const ren::MeshRef &mesh_ref, ePrimitiveType prim_type, eLayout layout) {
    using namespace LMapperInternal;

    const ren::Mesh *m = mesh_ref.get();

    const float *attribs = reinterpret_cast<const float *>(m->attribs());
    const unsigned short *indices = reinterpret_cast<const unsigned short *>(m->indices());

    unsigned start_index = (unsigned)vertices_.size();

    ////////////////////////

    std::vector<unsigned> triangle_list;

    bool is_odd = true;

    unsigned v0 = indices[0],
             v1 = indices[1],
             v2;

    for (unsigned i = 2; i < m->indices_size() / 2; i++) {
        is_odd = !is_odd;

        v2 = indices[i];

        if (v0 != v1 && v1 != v2) {
            triangle_list.push_back(v0);
            triangle_list.push_back(v1);
            triangle_list.push_back(v2);
        }

        std::swap(is_odd ? v1 : v0, v2);
    }

    unsigned root_node_index = (unsigned)bvh_nodes_.size();

    std::list<std::vector<unsigned>> triangle_lists;
    triangle_lists.push_back(std::move(triangle_list));

    unsigned num_nodes = (unsigned)bvh_nodes_.size();

    while (!triangle_lists.empty()) {
        auto split_data = SplitPrimitives(attribs, layout, triangle_lists.back());
        triangle_lists.pop_back();

        unsigned leaf_index = (unsigned)bvh_nodes_.size(),
                 escape_index = 0;

        for (unsigned i = leaf_index - 1; i >= root_node_index; i--) {
            if (bvh_nodes_[i].left_child == leaf_index) {
                escape_index = bvh_nodes_[i].right_child;
                break;
            } else if (bvh_nodes_[i].right_child == leaf_index) {
                leaf_index = i;
            }
        }

        if (split_data.divide_index == 0) {
            bvh_nodes_.push_back({ (unsigned)vtx_indices_.size(), (unsigned)split_data.indices.size(), 0, 0, escape_index,
                                 { split_data.left_bounds[0], split_data.left_bounds[1] } });
            for (unsigned i : split_data.indices) {
                vtx_indices_.push_back(start_index + i);
            }
        } else {
            unsigned index = num_nodes;
            bvh_nodes_.push_back({ 0, 0, index + 1, index + 2, escape_index,
                            { glm::min(split_data.left_bounds[0], split_data.right_bounds[0]),
                              glm::max(split_data.left_bounds[1], split_data.right_bounds[1]) } });
            std::vector<unsigned> left(split_data.indices.begin(), split_data.indices.begin() + split_data.divide_index),
                                  right(split_data.indices.begin() + split_data.divide_index, split_data.indices.end());
            assert(left.size() + right.size() == split_data.indices.size());
            triangle_lists.push_front(std::move(right));
            triangle_lists.push_front(std::move(left));
            
            num_nodes += 2;
        }
    }

    for (unsigned i = 0; i < m->attribs_size() / (8 * sizeof(float)); i++) {
        AddVertex(MakeVertex(&attribs[i * 8], layout));
    }

    printf("%s:\n", m->name());
    printf("\tBVH nodes: %u\n", num_nodes);

    ////////////////////////

    meshes_.push_back({ root_node_index });
    return (unsigned)meshes_.size() - 1;
}

unsigned lm::LMapper::AddPoly(const float *attribs, const unsigned *indices, unsigned count,
                              const ren::Texture2DRef &tex, eLayout layout) {
    using namespace LMapperInternal;

    const float lmap_scale = 1.0f / lightmap_res_;

    unsigned start_index = (unsigned)vtx_indices_.size(),
             num_indices = 0;

    unsigned lmap_index = 0;

    glm::vec3 normal, up, side;
    {   Vertex v1 = MakeVertex(&attribs[indices[0] * 8], layout),
        v2 = MakeVertex(&attribs[indices[1] * 8], layout),
        v3 = MakeVertex(&attribs[indices[2] * 8], layout);

        glm::vec3 e1 = v3.p - v1.p,
                  e2 = v2.p - v1.p;

        normal = glm::normalize(glm::cross(e1, e2));

        if (glm::abs(normal.y) <= glm::abs(normal.x) && glm::abs(normal.y) <= glm::abs(normal.z)) {
            up = { 0, 1, 0 };
        } else if (glm::abs(normal.x) <= glm::abs(normal.y) && glm::abs(normal.x) <= glm::abs(normal.z)) {
            up = { 1, 0, 0 };
        } else {
            up = { 0, 0, 1 };
        }

        side = glm::normalize(glm::cross(normal, up));
        up = glm::cross(side, normal);
    }

    glm::vec2 uv_min = glm::vec2{ std::numeric_limits<float>::max() },
              uv_max = glm::vec2{ std::numeric_limits<float>::lowest() };

    for (unsigned i = 0; i < count; i++) {
        Vertex v = MakeVertex(&attribs[indices[i] * 8], layout);

        v.uv = { glm::dot(v.p, side), glm::dot(v.p, up) };
        v.uv *= scale_;
        uv_min = glm::min(uv_min, v.uv);
        uv_max = glm::max(uv_max, v.uv);

        vtx_indices_.push_back(AddVertex(v));
        num_indices++;
    }

    glm::ivec2 size = uv_max - uv_min;
    size += glm::ivec2(2, 2); // 2px padding
    glm::ivec2 lmap_pos = { -1, 0 };

    for (unsigned i = 0; i < lightmaps_.size(); i++) {
        if (lightmaps_[i].FitBlock(size.x, size.y, lmap_pos.x, lmap_pos.y)) {
            lmap_index = i;
            break;
        }
    }

    if (lmap_pos.x == -1) {
        lightmaps_.emplace_back(lightmap_res_);
        bool res = lightmaps_.back().FitBlock(size.x, size.y, lmap_pos.x, lmap_pos.y);
        assert(res);
        lmap_index = (unsigned)lightmaps_.size() - 1;
    }

    glm::vec2 uv_move = lmap_pos;
    uv_move -= uv_min;

    for (unsigned i = start_index; i < start_index + num_indices; i++) {
        Vertex &v = vertices_[vtx_indices_[i]];
        v.uv = (v.uv + uv_move) * lmap_scale;
    }

    polys_.push_back({ start_index, num_indices, lmap_index });
    ProjectPoly(polys_.back(), tex, lightmaps_[lmap_index]);
    return (unsigned)polys_.size() - 1;
}

unsigned lm::LMapper::AddMeshInstance(unsigned id, const glm::mat4 &transform) {
    mesh_instances_.push_back({ id, {}, transform });
    return (unsigned)mesh_instances_.size() - 1;
}

unsigned lm::LMapper::AddLightPoint(const glm::vec3 &pos, float radius, const glm::vec3 &col) {
    point_lights_.push_back({ pos, col, radius });
    return (unsigned)point_lights_.size() - 1;
}

void lm::LMapper::MoveMeshInstance(unsigned id, const glm::vec3 &v) {
    auto &m = mesh_instances_[id];
    m.transform = glm::translate(m.transform, v);
}

void lm::LMapper::RotateMeshInstance(unsigned id, const glm::vec3 &v) {
    auto &m = mesh_instances_[id];
    m.transform = glm::rotate(m.transform, glm::radians(v.z), glm::vec3(0, 0, 1));
    m.transform = glm::rotate(m.transform, glm::radians(v.x), glm::vec3(1, 0, 0));
    m.transform = glm::rotate(m.transform, glm::radians(v.y), glm::vec3(0, 1, 0));
}

void lm::LMapper::TransformMeshInstance(unsigned id, const glm::mat4 &mat) {
    auto &m = mesh_instances_[id];
    m.transform = mat;
}

unsigned lm::LMapper::AddVertex(const Vertex &v) {
    vertices_.push_back(v);
    return (unsigned)vertices_.size() - 1;
}

