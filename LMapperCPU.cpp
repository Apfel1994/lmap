#include "LMapper.h"

#include <cstdio>
#include <cstring>
#include <ctime>
#include <limits>

#include "Common.h"

namespace LMapperInternal {
    struct RandState {
        unsigned next1 = 1151752134u, next2 = 2070363486u;
    } rand_state;

    inline unsigned rand() {
        rand_state.next1 = rand_state.next1 * 1701532575u + 571550083u;
        rand_state.next2 = rand_state.next2 * 3145804233u + 4178903934u;
        return (rand_state.next1 << 16) ^ rand_state.next2;
    }

    inline float U_m1_p1() {
        return float(LMapperInternal::rand())*(1.0f / 2147483648.0f) - 1.0f;
    }

    inline float U_0_p1() {
        return float(LMapperInternal::rand())*(0.5f / 2147483648.0f);
    }

    inline glm::vec3 pick_random_point_in_sphere() {
        float x0, x1, x2, x3, d2;
        do {
            x0 = U_m1_p1();
            x1 = U_m1_p1();
            x2 = U_m1_p1();
            x3 = U_m1_p1();
            d2 = x0*x0 + x1*x1 + x2*x2 + x3*x3;
        } while (d2 > 1.0f);
        float scale = 1.0f / d2;
        return glm::vec3(2 * (x1*x3 + x0*x2)*scale,
                         2 * (x2*x3 + x0*x1)*scale,
                         (x0*x0 + x3*x3 - x1*x1 - x2*x2)*scale);
    }

    inline vec3 pick_random_point_in_hemisphere(const vec3 &v) {
        vec3 result = pick_random_point_in_sphere();
        if (dot(result, v) < 0) {
            result.x = -result.x;
            result.y = -result.y;
            result.z = -result.z;
        }
        return result;
    }

    const float eps = std::numeric_limits<float>::epsilon() * 4;
    const float max_diff = 0.025f;
}

#include <chrono>

void lm::LMapper::InvalidatePreview() {
    preview_iteration_ = 0;
    samples_done_ = 0;
}

const unsigned char *lm::LMapper::preview_pixels() {
    if (preview_pixels_dirty_) {
#pragma omp parallel for
        for (int i = 0; i < (int)preview_buffer_.size(); i++) {
            glm::vec3 cur_col = preview_buffer_[i];
            cur_col = glm::pow(cur_col, glm::vec3(1 / 2.2f));

            preview_pixels_[4 * i + 0] = (unsigned char)(255 * clamp(cur_col[0], 0.0f, 1.0f));
            preview_pixels_[4 * i + 1] = (unsigned char)(255 * clamp(cur_col[1], 0.0f, 1.0f));
            preview_pixels_[4 * i + 2] = (unsigned char)(255 * clamp(cur_col[2], 0.0f, 1.0f));
        }
        preview_pixels_dirty_ = false;
    }
    return &preview_pixels_[0];
}

void lm::LMapper::UpdatePreview(int w, int h, const glm::vec3 &origin, const glm::vec3 &dir, const glm::vec3 &side, const glm::vec3 &up) {
    using namespace LMapperInternal;

    if (w * h * 4 != preview_buffer_.size()) {
        preview_buffer_.resize((size_t)w * h);
        preview_pixels_.resize((size_t)w * h * 4);
    }

    preview_pixels_dirty_ = true;
    preview_iteration_++;

    int per_pixel_samples = (int)(1 + 0.1f * preview_iteration_);
    per_pixel_samples = glm::min(per_pixel_samples, 1);

    samples_done_ += per_pixel_samples;

    int step = 8 / preview_iteration_;
    step = max(step, 1);
    //step = 1;

#pragma omp parallel for private(rand_state) schedule(dynamic)
    for (int y = 0; y < h; y += step) {
        for (int x = 0; x < w; x += step) {
            vec3 _d(float(x) / w - 0.5f, float(-y) / h + 0.5f, 1);
            _d = _d.x * side + _d.y * up + _d.z * dir;
            //
            float total_weight = 0;
            vec3 color;

            //auto ___t1 = std::chrono::high_resolution_clock::now();

            for (int i = 0; i < per_pixel_samples; i++) {
                float dx = 0.5f * U_m1_p1(),
                      dy = 0.5f * U_m1_p1();
                float weight = std::abs(dx) > std::abs(dy) ? (1 - std::abs(dx)) : (1 - std::abs(dy));
                total_weight += weight;
                vec3 d = _d + (dx / w) * side + (dy / h) * up; //vec3(dx / w, dy / h, 0);
                d = normalize(d);

                Ray ray = { origin, d };

                Vertex v;
                unsigned poly = std::numeric_limits<unsigned>::max();
                float dist = Trace(ray, &v, &poly);
                if (dist > eps /*&& dot(v.n, ray.dir) <= eps*/) {
                    if (stage_ == Done) {
                        if (poly != std::numeric_limits<unsigned>::max()) {
                            const auto &p = polys_[poly];
                            color += lightmaps_[p.lmap_index].light_lerp(v.uv.x, v.uv.y) * weight;
                            //color += glm::abs(lightmaps_[p.lmap_index].pos_lerp(v.uv.x, v.uv.y) - v.p) * weight;
                            //color += lightmaps_[p.lmap_index].pos(v.uv.x, v.uv.y) * weight;
                            //color += v.p * weight;
                        }
                    } else {
                        color += (diffuse(v) + gather(v, 1, 1)) * shade(v, poly) * weight;
                    }
                    //color += (0.5f * v.n + glm::vec3(0.5f)) * weight;
                }
            }

            //auto ___t2 = std::chrono::high_resolution_clock::now();

            if (total_weight > 0) {
                glm::vec3 *cur_col = &preview_buffer_[y * w + x];
                glm::vec3 new_col = color / total_weight;

                //auto dt = std::chrono::duration_cast<std::chrono::nanoseconds>(___t2 - ___t1).count();
                //new_col.r += 0.000025f * dt;

                new_col = (glm::vec3)*cur_col + glm::vec3(new_col - *cur_col) / (float)preview_iteration_;

                for (int _y = y; _y < min(y + step, h); _y++) {
                    for (int _x = x; _x < min(x + step, w); _x++) {
                        preview_buffer_[_y * w + _x] = new_col;
                    }
                }
            }
        }
    }
}

void lm::LMapper::ProcessLightmap() {
    using namespace LMapperInternal;

    for (auto &lmap : lightmaps_) {
        auto final_gather = [this, &lmap](int x, int y) {
            float dx = U_0_p1(), dy = U_0_p1();
            float weight = std::abs(dx) > std::abs(dy) ? (1 - std::abs(dx)) : (1 - std::abs(dy));
            float _x = (x + dx) / lightmap_res_,
                  _y = 1 - (y + dy) / lightmap_res_;

            const vec3 &p = lmap.pos_lerp(_x, _y),
                       &n = lmap.normal_lerp(_x, _y);

            const Vertex v = { p + eps * n, n, {} };
            vec3 col = diffuse(v);

            vec3 dir = pick_random_point_in_hemisphere(v.n);
            Ray ray = { v.p, dir };
            Vertex res;
            unsigned poly_index = std::numeric_limits<unsigned>::max();
            if (Trace(ray, &res, &poly_index) > eps && !lmap.empty(res.uv.x, res.uv.y)) {
                col += weight * dot(dir, v.n) * lmap.light_cache(res.uv.x, res.uv.y);
            }

            lmap.incr_light(x, y, col);
        };

        if (stage_ == PrepareMaps) {
            lmap.FlushSeams(lm::PosMap | lm::NormalMap | lm::AlbedoMap);
            stage_ = LightCache;
        } else if (stage_ == LightCache) {
            lmap.incr_samples();

#pragma omp parallel for private(rand_state)
            for (int y = 0; y < lightmap_res_; y++) {
                for (int x = 0; x < lightmap_res_; x++) {
                    if (lmap.empty(x, y)) continue;

                    const vec3 &p = lmap.pos(x, y);
                    const vec3 &n = lmap.normal(x, y);

                    const Vertex &v = { p + eps * n, n, {} };
                    vec3 col = diffuse(v) + gather(v, 1, 1);

                    lmap.incr_light(x, y, col);
                }
            }

            printf("\rLightcache samples: %u\n", lmap.samples());
            if (lmap.samples() >= light_cache_samples_) {
                lmap.ProcessLightCache();
                lmap.ClearLight();
                stage_ = FinalGather0;
            }
        } else if (stage_ == FinalGather0) {
            lmap.incr_samples();

#pragma omp parallel for private(rand_state)
            for (int y = 0; y < lightmap_res_; y += 2) {
                for (int x = 0; x < lightmap_res_; x += 2) {
                    if (!lmap.empty(x, y)) {
                        final_gather(x, y);
                    }
                }
            }

            printf("Final gather pass1 samples: %u\n", lmap.samples());
            if (lmap.samples() >= final_gather_samples_ * 4) {
                lmap.set_samples(0);
                stage_ = FinalGather1;
            }
        } else if (stage_ == FinalGather1) {
            lmap.incr_samples();

#pragma omp parallel for private(rand_state)
            for (int y = 1; y < lightmap_res_; y += 2) {
                for (int x = 1; x < lightmap_res_; x += 2) {
                    if (lmap.empty(x, y)) continue;

                    int x0 = x - 1, x1 = x + 1, y0 = y - 1, y1 = y + 1;
                    x0 = x0 < 0 ? 0 : x0;
                    x1 = x1 > lightmap_res_ - 1 ? lightmap_res_ - 1 : x1;
                    y0 = y0 < 0 ? 0 : y0;
                    y1 = y1 > lightmap_res_ - 1 ? lightmap_res_ - 1 : y1;

                    const vec3 &c0 = lmap.light(x0, y0),
                               &c1 = lmap.light(x1, y0),
                               &c2 = lmap.light(x1, y1),
                               &c3 = lmap.light(x0, y1);

                    bool force = lmap.empty(x0, y0) || lmap.empty(x1, y0) || lmap.empty(x1, y1) || lmap.empty(x0, y1);

                    if (force || (glm::distance2(c0, c2) + glm::distance2(c1, c3)) > max_diff) {
                        final_gather(x, y);
                    } else {
                        lmap.set_light(x, y, 0.25f * (c0 + c1 + c2 + c3));
                    }
                }
            }

#pragma omp parallel for private(rand_state)
            for (int y = 0; y < lightmap_res_; y++) {
                int start = y % 2 == 0;
                for (int x = start; x < lightmap_res_; x += 2) {
                    if (lmap.empty(x, y)) continue;

                    int x0 = x - 1, x1 = x + 1, y0 = y - 1, y1 = y + 1;
                    x0 = x0 < 0 ? 0 : x0;
                    x1 = x1 > lightmap_res_ - 1 ? lightmap_res_ - 1 : x1;
                    y0 = y0 < 0 ? 0 : y0;
                    y1 = y1 > lightmap_res_ - 1 ? lightmap_res_ - 1 : y1;

                    const vec3 &c0 = lmap.light(x0, y),
                               &c1 = lmap.light(x1, y),
                               &c2 = lmap.light(x, y0),
                               &c3 = lmap.light(x, y1);

                    bool force = lmap.empty(x0, y) || lmap.empty(x1, y) || lmap.empty(x, y0) || lmap.empty(x, y1);

                    if (force || (glm::distance2(c0, c2) + glm::distance2(c1, c3)) > max_diff) {
                        final_gather(x, y);
                    } else {
                        lmap.set_light(x, y, 0.25f * (c0 + c1 + c2 + c3));
                    }
                }
            }

            printf("Final gather pass2 samples: %u\n", lmap.samples());
            if (lmap.samples() >= final_gather_samples_) {
                lmap.FlushSeams(lm::LightMap);
                stage_ = Done;
            }
        } else if (stage_ == Done) {

        }
    }
}

float lm::LMapper::Trace(const Ray &ray, Vertex *out_v, unsigned *poly_index) const {
    using namespace LMapperInternal;

    const Vertex *pv1 = nullptr, *pv2, *pv3;

    vec4 best_intersection;
    best_intersection.w = std::numeric_limits<float>::max();

    for (unsigned poly = 0; poly < (unsigned)polys_.size(); poly++) {
        const Poly &p = polys_[poly];
        unsigned p1 = p.start_index;
        for (unsigned i = p.start_index + 1; i < p.start_index + p.num_vertices - 1; i++) {
            unsigned p2 = i, p3 = i + 1;
            const Vertex &v1 = vertices_[vtx_indices_[p1]],
                         &v2 = vertices_[vtx_indices_[p2]],
                         &v3 = vertices_[vtx_indices_[p3]];
            vec4 intersection;
            if (IntersectTriangle(ray, v1.p, v2.p, v3.p, &intersection) &&
                abs(intersection.w) < abs(best_intersection.w)) {
                best_intersection = intersection;
                pv1 = &v1; pv2 = &v2; pv3 = &v3;
                if (poly_index) *poly_index = poly;
                break;
            }
        }
    }

    for (const auto &m : mesh_instances_) {
        const auto &mesh = meshes_[m.mesh_id];

        glm::mat4 inv_mat = glm::inverse(m.transform);
        Ray tr_ray = { vec3(inv_mat * vec4(ray.origin, 1.0f)), vec3(inv_mat * vec4(ray.dir, 0.0f)) };

        unsigned cur_node = mesh.node_index;
        while (cur_node) {
            const auto &node = bvh_nodes_[cur_node];

            float dist;
            if (!IntersectBbox(tr_ray, node.bbox[0], node.bbox[1], &dist) || dist > abs(best_intersection.w)) {
                cur_node = node.escape_index;
                continue;
            }

            if (node.num_indices) {
                for (unsigned i = node.start_index; i < node.start_index + node.num_indices; i += 3) {
                    const Vertex *vert0 = &vertices_[vtx_indices_[i]],
                                 *vert1 = &vertices_[vtx_indices_[i + 1]],
                                 *vert2 = &vertices_[vtx_indices_[i + 2]];

                    vec4 in;
                    if (IntersectTriangle(tr_ray, vert0->p, vert1->p, vert2->p, &in) && abs(in.w) < abs(best_intersection.w)) {
                        best_intersection = in;
                        pv1 = vert0; pv2 = vert1; pv3 = vert2;
                        if (poly_index) *poly_index = 0;
                    }
                }

                cur_node = node.escape_index;
            } else {
                cur_node = node.left_child;
            }
        }
    }

    if (!pv1) return 0;

    if (out_v) {
        out_v->p = best_intersection.x * pv1->p + best_intersection.y * pv2->p + best_intersection.z * pv3->p;
        out_v->uv = best_intersection.x * pv1->uv + best_intersection.y * pv2->uv + best_intersection.z * pv3->uv;
        out_v->n = best_intersection.x * pv1->n + best_intersection.y * pv2->n + best_intersection.z * pv3->n;
    }
    return best_intersection.w;
}

float lm::LMapper::trace(const struct Ray &ray) const {
    float d = Trace(ray);
    return d == std::numeric_limits<float>::max() ? 0 : d;
}

glm::vec3 lm::LMapper::diffuse(const Vertex &v) const {
    using namespace LMapperInternal;

    glm::vec3 ret;
    const int num_samples = 1;
    for (const PointLight &l : point_lights_) {
        for (int i = 0; i < num_samples; i++) {
            vec3 ll = v.p - l.pos;
            vec3 r = pick_random_point_in_hemisphere(ll);
            r *= l.radius;

            vec3 light_dir = (l.pos + r) - v.p;
            const float d_sqr = glm::length2(light_dir);
            light_dir /= glm::sqrt(d_sqr);

            float _dot = dot(light_dir, v.n);
            if (_dot < eps) continue;

            Ray light_ray = { v.p + eps * light_dir, light_dir };
            float d = trace(light_ray);
            
            if (d > -eps && (d < eps || d * d > d_sqr - eps * eps)) {
                ret += l.col * _dot / (1 + d_sqr);
            }
        }
    }

    return ret / (float)num_samples;
}

glm::vec3 lm::LMapper::shade(const Vertex &v, unsigned poly_index) const {
    static vec3 mat_colors[] = { { 1, 1, 1 }, { 1, 0, 0 }, { 1, 1, 1 }, { 0, 1, 0 }, { 1, 1, 1 }, { 1, 1, 1 }, { 0, 0, 1 } };
    //return mat_colors[poly_index % 7];
    const float k = 1.0f / 255;
    return k * (vec3)lightmaps_[0].albedo(v.uv.x, v.uv.y);
}

glm::vec3 lm::LMapper::gather(const Vertex &v, int cur_depth, int num_samples) const {
    using namespace LMapperInternal;

    glm::vec3 ret;
    if (cur_depth <= ray_depth_) {
        for (int i = 0; i < max(num_samples / cur_depth, 1); i++) {
            glm::vec3 dir = pick_random_point_in_hemisphere(v.n);
            Ray ray = { v.p + eps * v.n, dir };
            Vertex res;
            unsigned poly_index;
            if (Trace(ray, &res, &poly_index) > eps) {
                ret += dot(dir, v.n) * (diffuse(res) + gather(res, cur_depth + 1, num_samples)) * shade(res, poly_index);
            }
        }
    }
    //const float k = 0.5f / glm::pi<float>();
    return ret / (float)num_samples;
}