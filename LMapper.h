#pragma once

#include <vector>

#include <glm/mat4x4.hpp>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>

#include <ren/Fwd.h>

#include "LMap.h"

struct Ray;

namespace lm {
    enum eLayout { PxyzNxyzTuv };
    enum ePrimitiveType { TriangleList, TriangleStrip };
    enum eLightmapStage { PrepareMaps, LightCache, FinalGather0, FinalGather1, Done };

    struct Vertex {
        glm::vec3 p;
        glm::vec3 n;
        glm::vec2 uv;
    };
    static_assert(sizeof(Vertex) == 32, "!");

    struct Poly {
        unsigned start_index, num_vertices;
        unsigned lmap_index;
    };

    struct BVHNode {
        unsigned start_index, num_indices,
                 left_child, right_child,
                 escape_index;
        glm::vec3 bbox[2];
    };

    struct Mesh {
        unsigned node_index;
    };

    struct MeshInstance {
        unsigned mesh_id;
        glm::vec4 lmap_offset;
        glm::mat4 transform;
    };

    class LMapper {
    public:
        LMapper(int res = 256);
        ~LMapper();

        unsigned LoadMesh(const ren::MeshRef &mesh_ref, ePrimitiveType prim_type, eLayout layout);

        unsigned AddPoly(const float *attribs, const unsigned *indices, unsigned count,
                         const ren::Texture2DRef &tex, eLayout layout);
        unsigned AddMeshInstance(unsigned id, const glm::mat4 &transform = glm::mat4());
        unsigned AddLightPoint(const glm::vec3 &pos, float radius, const glm::vec3 &col);

        void MoveMeshInstance(unsigned id, const glm::vec3 &v);
        void RotateMeshInstance(unsigned id, const glm::vec3 &v);
        void TransformMeshInstance(unsigned id, const glm::mat4 &mat);

        const unsigned char *preview_pixels();

        int ray_depth() const { return ray_depth_; }

        unsigned samples_done() const { return samples_done_; }

        void set_ray_depth(int depth) { ray_depth_ = depth; }
        void set_scale(float s) { scale_ = s; }
        void set_light_cache_samples(unsigned s) { light_cache_samples_ = s; }
        void set_final_gather_samples(unsigned s) { final_gather_samples_ = s; }

        void InvalidatePreview();

        void UpdatePreview(int w, int h, const glm::vec3 &origin, const glm::vec3 &dir, const glm::vec3 &side,
                           const glm::vec3 &up);

        void UpdateUVsPreview();

        void ProcessLightmap();

        eLightmapStage stage() const { return stage_; }
    private:
        std::vector<Vertex> vertices_;
        std::vector<unsigned> vtx_indices_;
        std::vector<Poly> polys_;

        std::vector<BVHNode> bvh_nodes_;

        std::vector<Mesh> meshes_;
        std::vector<MeshInstance> mesh_instances_;

        struct PointLight {
            glm::vec3 pos, col;
            float radius;
        };
        std::vector<PointLight> point_lights_;

        eLightmapStage stage_;
        unsigned light_cache_samples_, final_gather_samples_;
        std::vector<lm::LMap> lightmaps_;
        float scale_;

#if defined(USE_SW_RENDER)
        int32_t f32_framebuffer_, u8_framebuffer_,
                bake_attr_program_, bake_albedo_program_;
#endif

        void Init();

        int ray_depth_;
        int lightmap_res_;

        int prev_step_;
        std::vector<glm::vec3> preview_buffer_;
        std::vector<unsigned char> preview_pixels_;
        unsigned preview_iteration_, samples_done_;
        bool preview_pixels_dirty_;

        unsigned AddVertex(const Vertex &v);
        void ProjectPoly(const Poly &p, const ren::Texture2DRef &tex, lm::LMap &lmap);

        struct SplitData {
            std::vector<unsigned> indices;
            unsigned divide_index;
            glm::vec3 left_bounds[2], right_bounds[2];
        };
        SplitData SplitPrimitives(const float *attrs, eLayout layout, const std::vector<unsigned> &indices);

        float Trace(const Ray &ray, Vertex *out_v = nullptr, unsigned *poly_index = nullptr) const;
        float trace(const Ray &ray) const;

        glm::vec3 diffuse(const Vertex &v) const;
        glm::vec3 shade(const Vertex &v, unsigned poly_index) const;
        glm::vec3 gather(const Vertex &v, int cur_depth, int num_samples) const;
    };
}
