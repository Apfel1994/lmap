#undef NDEBUG
#include <cassert>

#include <glm/ext.hpp>

#include "../LMap.h"

void test_lmap() {
    const float eps = 0.001f;

    {   // test lerp
        lm::LMap l(16);
        l.set_pos(0, 0, { 0, 0, 0 });
        l.set_pos(0, 1, { 2, 1, 0.5f });
        l.set_pos(1, 0, { 2, 1, 0.5f });
        l.set_pos(1, 1, { 2, 1, 0.5f });

        auto d = l.pos_lerp(0.0f / 16, 1 - 0.5f / 16);
        assert(glm::distance(d, glm::vec3{ 1, 0.5f, 0.25f }) < eps);
    }
}