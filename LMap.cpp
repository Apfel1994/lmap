#include "LMap.h"

bool lm::LMap::FitBlock(int w, int h, int &out_x, int &out_y) {
    int best, best2;

    best = res_;
    for (int i = 0; i < res_ - w; i++) {
        best2 = 0;
        int j;
        for (j = 0; j < w; j++) {
            if (heights_[i + j] >= best) {
                break;
            }
            if (heights_[i + j] > best2) {
                best2 = heights_[i + j];
            }
        }
        if (j == w) {
            out_x = i;
            out_y = best = best2;
        }
    }

    if (best + h > res_) {
        return false;
    }

    for (int i = 0; i < w; i++) {
        heights_[out_x + i] = (short)(best + h);
    }
    return true;
}

void lm::LMap::ProcessLightCache() {
    const float gauss[] = { 0.2270270270f, 0.1945945946f, 0.1216216216f, 0.0540540541f, 0.0162162162f };
    const float gauss_left_sum[] = { 0, 0.3864865005f, 0.1918918937f, 0.07027027011f, 0.0162162162f };

    std::vector<glm::vec3> temp((size_t)res_ * res_);

    for (int y = 0; y < res_; y++) {
        for (int i = 0; i < res_; i++) {
            if (empty(i, y)) continue;
            int last = 0;
            temp[y * res_ + i] += gauss[0] * light_[y * res_ + i];
            for (int j = 1; j <= 4; j++) {
                int x = glm::clamp(i + j, 0, res_ - 1);
                float k = gauss[j];
                if (empty(x, y)) {
                    temp[y * res_ + i] += gauss_left_sum[j] * light_[y * res_ + last];
                    break;
                } else {
                    temp[y * res_ + i] += k * light_[y * res_ + x];
                    last = x;
                }
            }
            last = 0;
            for (int j = -1; j >= -4; j--) {
                int x = glm::clamp(i + j, 0, res_ - 1);
                float k = gauss[-j];
                if (empty(x, y)) {
                    temp[y * res_ + i] += gauss_left_sum[-j] * light_[y * res_ + last];
                    break;
                } else {
                    temp[y * res_ + i] += k * light_[y * res_ + x];
                    last = x;
                }
            }
        }
    }

    std::fill(light_cache_.begin(), light_cache_.end(), glm::vec3());

    for (int x = 0; x < res_; x++) {
        for (int i = 0; i < res_; i++) {
            if (empty(x, i)) continue;
            int last = 0;
            light_cache_[i * res_ + x] += gauss[0] * temp[i * res_ + x];
            for (int j = 1; j <= 4; j++) {
                int y = glm::clamp(i + j, 0, res_ - 1);
                float k = gauss[j];
                if (empty(x, y)) {
                    light_cache_[i * res_ + x] += gauss_left_sum[j] * temp[last * res_ + x];
                    break;
                } else {
                    light_cache_[i * res_ + x] += k * temp[y * res_ + x];
                    last = y;
                }
            }
            last = 0;
            for (int j = -1; j >= -4; j--) {
                int y = glm::clamp(i + j, 0, res_ - 1);
                float k = gauss[-j];
                if (empty(x, y)) {
                    light_cache_[i * res_ + x] += gauss_left_sum[-j] * temp[last * res_ + x];
                    break;
                } else {
                    light_cache_[i * res_ + x] += k * temp[y * res_ + x];
                    last = y;
                }
            }
        }
    }

    FlushSeams(LightCacheMap);

    const float k = 1.0f / 255;

    for (int y = 0; y < res_; y++) {
        for (int x = 0; x < res_; x++) {
            light_cache_[y * res_ + x] *= k * (glm::vec3)albedo_[y * res_ + x];
        }
    }
}

void lm::LMap::FlushSeams(int maps) {
    for (int y = 0; y < res_; y++) {
        for (int x = 0; x < res_; x++) {
            if (empty(x, y)) {
                int x0 = x - 1, x1 = x + 1,
                    y0 = y - 1, y1 = y + 1;

                x0 = glm::clamp(x0, 0, res_ - 1);
                x1 = glm::clamp(x1, 0, res_ - 1);
                y0 = glm::clamp(y0, 0, res_ - 1);
                y1 = glm::clamp(y1, 0, res_ - 1);

                glm::vec3 p, n, l, al, lc;
                float num_cols = 0;

                glm::ivec2 pp[] = { { x0, y0 }, { x, y0 }, { x1, y0 }, { x1, y }, { x1, y1 }, { x, y1 }, { x0, y1 }, { x0, y } };

                for (const auto &_ : pp) {
                    if (!empty(_.x, _.y)) {
                        if (maps & PosMap) p += pos(_.x, _.y);
                        if (maps & NormalMap) n += normal(_.x, _.y);
                        if (maps & LightMap) l += light(_.x, _.y);
                        if (maps & AlbedoMap) al += albedo(_.x, _.y);
                        if (maps & LightCacheMap) lc += light_cache(_.x, _.y);

                        num_cols++;
                    }
                }

                if (num_cols) {
                    if (maps & PosMap) set_pos(x, y, p / num_cols);
                    if (maps & NormalMap) set_normal(x, y, n / num_cols);
                    if (maps & LightMap) set_light(x, y, l / num_cols);
                    if (maps & AlbedoMap) set_albedo(x, y, al / num_cols);
                    if (maps & LightCacheMap) set_light_cache(x, y, lc / num_cols);
                }
            }
        }
    }
}