#pragma once

#include <cmath>

#include <glm/geometric.hpp>
#include <glm/gtx/norm.hpp>
#include <glm/vec3.hpp>

/*using vec3 = glm::vec3;
using glm::clamp;
using glm::dot;
using glm::normalize;*/

using namespace glm;

struct Ray {
    vec3 origin, dir;
};

struct Sphere {
    vec3 center;
    float radius;

    Sphere(const vec3 &_center, float _radius) : center(_center), radius(_radius) {}
};

inline float Intersect(const Ray &r, const Sphere &sph) {
    const float eps = (float)1e-4;
    vec3 op = sph.center - r.origin;
    float b = dot(op, r.dir),
          det = b * b - dot(op, op) + sph.radius * sph.radius;
    if (det < 0) {
        return 0;
    } else {
        det = sqrtf(det);
    }
    float t = b - det;
    if (t > eps) {
        return t;
    } else {
        t = b + det;
        if (t > eps) {
            return t;
        } else {
            return 0;
        }
    }
}

inline float Randf() {
    return (float)rand() / RAND_MAX;
}

/////////////////////////////////////////

/*inline bool IntersectTriangle(const Ray &ray, const vec3 &pos0, const vec3 &pos1, const vec3 &pos2, vec4 *uvwt) {
    const float eps = 0.00001f;

    vec3 E1 = pos1 - pos0;
    vec3 E2 = pos2 - pos0;

    vec3 P = cross(-ray.dir, E2);

    float det = dot(P, E1);

    if (det < eps && det > -eps) {
        return false;
    }

    vec3 T = ray.origin - pos0;

    float inv_det = 1 / det;

    float u = dot(P, T) * inv_det;

    if (u < 0 || u > 1) {
        return false;
    }

    vec3 Q = cross(T, E1);

    float v = dot(Q, -ray.dir) * inv_det;

    if (v < 0 || u + v > 1) {
        return false;
    }

    float t = -dot(Q, E2) * inv_det;

    if (t < 0) {
        return false;
    }

    *uvwt = vec4(1 - u - v, u, v, t);

    return true;
}*/

inline bool IntersectTriangle(const Ray &ray, const vec3 &pos0, const vec3 &pos1, const vec3 &pos2, vec4 *uvwt) {
    const float eps = 0.000001f;

    vec3 E1 = pos1 - pos0;
    vec3 E2 = pos2 - pos0;

    vec3 P = cross(-ray.dir, E2);

    float det = dot(P, E1);

    //if (det < eps && det > -eps) {
    if (abs(det) < eps) {
    //if (det < eps) {
        return false;
    }

    vec3 T = ray.origin - pos0;

    float inv_det = 1 / det;

    float u = dot(P, T) * inv_det;

    if (u < 0 || u > 1) {
        return false;
    }

    vec3 Q = cross(T, E1);

    float v = dot(Q, -ray.dir) * inv_det;

    if (v < 0 || u + v > 1) {
        return false;
    }

    float t = -dot(Q, E2) * inv_det;

    if (t < 0) {
        return false;
    }

    *uvwt = vec4(1 - u - v, u, v, glm::sign(det) * t);

    return true;
}

inline bool IntersectBbox(const Ray &ray, const glm::vec3 &bbox_min, const vec3 &bbox_max, float* dist) {
    vec3 inv_dir = 1.0f / ray.dir;

    float low = inv_dir[0] * (bbox_min[0] - ray.origin[0]);
    float high = inv_dir[0] * (bbox_max[0] - ray.origin[0]);
    float tmin = min(low, high);
    float tmax = max(low, high);

    low = inv_dir[1] * (bbox_min[1] - ray.origin[1]);
    high = inv_dir[1] * (bbox_max[1] - ray.origin[1]);
    tmin = max(tmin, min(low, high));
    tmax = min(tmax, max(low, high));

    low = inv_dir[2] * (bbox_min[2] - ray.origin[2]);
    high = inv_dir[2] * (bbox_max[2] - ray.origin[2]);
    tmin = max(tmin, min(low, high));
    tmax = min(tmax, max(low, high));

    *dist = tmin;

    return ((tmin <= tmax) && (tmax > 0.0f));
}
