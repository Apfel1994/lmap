#include "LMapper.h"

#include <glm/vec4.hpp>
#include <glm/ext.hpp>

#include <ren/SW/SW.h>

namespace LMapperInternal {
    const int A_POS = 0;
    const int A_VEC3_ATTR = 1;

    const int V_VEC3_ATTR = 0;

    VSHADER bake_vec3_attr_vs(VS_IN, VS_OUT) {
        using namespace glm;

        *(vec3 *)V_FVARYING(V_VEC3_ATTR) = make_vec3(V_FATTR(A_VEC3_ATTR));
        *(vec4*)V_POS_OUT = vec4(2.0f * make_vec2(V_FATTR(A_POS)) - vec2(1, 1), 0, 1);
    }

    FSHADER bake_vec3_attr_fs(FS_IN, FS_OUT) {
        using namespace glm;

        *(glm::vec4 *)F_COL_OUT = vec4(make_vec3(F_FVARYING_IN(V_VEC3_ATTR)), 1);
    }

    const int A_UVS = 1;

    VSHADER bake_albedo_vs(VS_IN, VS_OUT) {
        using namespace glm;

        *(vec2 *)V_FVARYING(A_UVS) = make_vec2(V_FATTR(A_UVS));
        *(vec4*)V_POS_OUT = vec4(2.0f * make_vec2(V_FATTR(A_POS)) - vec2(1, 1), 0, 1);
    }

    FSHADER bake_albedo_fs(FS_IN, FS_OUT) {
        using namespace glm;

        *(glm::vec4 *)F_COL_OUT = vec4(0.75f, 0.75f, 0.75f, 1);
    }
}

void lm::LMapper::Init() {
    using namespace LMapperInternal;

    f32_framebuffer_ = swCreateFramebuffer(SW_FRGBA, lightmap_res_, lightmap_res_, 0);
    u8_framebuffer_ = swCreateFramebuffer(SW_BGRA8888, lightmap_res_, lightmap_res_, 0);

    bake_attr_program_ = swCreateProgram();
    swUseProgram(bake_attr_program_);
    swInitProgram(bake_vec3_attr_vs, bake_vec3_attr_fs, 3);

    bake_albedo_program_ = swCreateProgram();
    swUseProgram(bake_albedo_program_);
    swInitProgram(bake_albedo_vs, bake_albedo_fs, 2);
}

void lm::LMapper::ProjectPoly(const Poly &p, const ren::Texture2DRef &tex, lm::LMap &lmap) {
    using namespace LMapperInternal;

#define DRAW_POLY(p) {                                                                          \
        unsigned p1 = p.start_index;                                                            \
        for (unsigned i = p.start_index + 1; i < p.start_index + p.num_vertices - 1; i++) {     \
            unsigned p2 = i, p3 = i + 1;                                                        \
            const unsigned inds[3] = { vtx_indices_[p1], vtx_indices_[p2], vtx_indices_[p3] };  \
            swDrawElements(SW_TRIANGLES, 3, SW_UNSIGNED_INT, &inds[0]);                         \
        }                                                                                       \
    }

    swBindFramebuffer(f32_framebuffer_);
    swClearColor(0, 0, 0, 0);

    swUseProgram(bake_attr_program_);

    swVertexAttribPointer(A_POS, 2 * sizeof(SWfloat), 8 * sizeof(SWfloat), (float *)&vertices_[0].uv[0]);

    {   // bake positions
        swVertexAttribPointer(A_VEC3_ATTR, 3 * sizeof(SWfloat), 8 * sizeof(SWfloat), (float *)&vertices_[0].p[0]);

        DRAW_POLY(p);

        const glm::vec4 *positions = (const glm::vec4 *)swGetPixelDataRef(f32_framebuffer_);

#pragma omp parallel for
        for (int y = 0; y < lightmap_res_; y++) {
            for (int x = 0; x < lightmap_res_; x++) {
                const glm::vec4 &pos = positions[y * lightmap_res_ + x];
                if (pos.w > 0) {
                    lmap.set_pos(x, y, glm::vec3(pos));
                    lmap.set_empty(x, y, false);
                }
            }
        }
    }

    {   // bake normals
        swVertexAttribPointer(A_VEC3_ATTR, 3 * sizeof(SWfloat), 8 * sizeof(SWfloat), (float *)&vertices_[0].n[0]);

        DRAW_POLY(p);

        const glm::vec4 *normals = (const glm::vec4 *)swGetPixelDataRef(f32_framebuffer_);

#pragma omp parallel for
        for (int y = 0; y < lightmap_res_; y++) {
            for (int x = 0; x < lightmap_res_; x++) {
                const glm::vec4 &n = normals[y * lightmap_res_ + x];
                if (n.w > 0) {
                    lmap.set_normal(x, y, glm::vec3(n));
                }
            }
        }
    }

    swBindFramebuffer(u8_framebuffer_);
    swClearColor(0, 0, 0, 0);

    swUseProgram(bake_albedo_program_);

    swVertexAttribPointer(A_POS, 2 * sizeof(SWfloat), 8 * sizeof(SWfloat), (float *)&vertices_[0].uv[0]);

    {   // bake albedo
        swVertexAttribPointer(A_UVS, 2 * sizeof(SWfloat), 8 * sizeof(SWfloat), (float *)&vertices_[0].uv[0]);

        DRAW_POLY(p);

        const glm::u8vec4 *colors = (const glm::u8vec4 *)swGetPixelDataRef(u8_framebuffer_);

#pragma omp parallel for
        for (int y = 0; y < lightmap_res_; y++) {
            for (int x = 0; x < lightmap_res_; x++) {
                const glm::u8vec4 &c = colors[y * lightmap_res_ + x];
                if (c.w > 0) {
                    lmap.set_albedo(x, y, glm::u8vec3(c));
                }
            }
        }
    }
}

void lm::LMapper::UpdateUVsPreview() {
    //if (stage_ == Done) return;

    if (lightmap_res_ * lightmap_res_ * 4 != preview_buffer_.size()) {
        preview_buffer_.resize((size_t)lightmap_res_ * lightmap_res_);
        preview_pixels_.resize((size_t)lightmap_res_ * lightmap_res_ * 4);
    }

    preview_pixels_dirty_ = true;

    assert(!lightmaps_.empty());
    const auto &lmap = lightmaps_[0];
    for (int y = 0; y < lightmap_res_; y++) {
        for (int x = 0; x < lightmap_res_; x++) {
            //preview_buffer_[y * lightmap_res_ + x] = (glm::vec3)lmap.albedo(x, y)/255.0f;//lmap.normal(x, y);
            //if (stage_ == LightCache) {
                preview_buffer_[y * lightmap_res_ + x] = lmap.light(x, y);
            //} else if (stage_ == FinalGather) {
                //preview_buffer_[y * lightmap_res_ + x] = lmap.light(x, y);
                //preview_buffer_[y * lightmap_res_ + x] = lmap.light_cache(x, y);
            //}
        }
    }
}