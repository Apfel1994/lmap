#include "LMapper.h"

#include <algorithm>

#include <glm/ext.hpp>

namespace LMapperInternal {
    const float empty_node_traverse_cost = 16;
}

lm::LMapper::SplitData lm::LMapper::SplitPrimitives(const float *attrs, eLayout layout, const std::vector<unsigned> &indices) {
    using namespace LMapperInternal;

    struct prim_t {
        unsigned v0, v1, v2;
        glm::vec3 bbox_min, bbox_max;
    };

    struct bbox_t {
        glm::vec3 min = glm::vec3{ std::numeric_limits<float>::max() },
                  max = glm::vec3{ std::numeric_limits<float>::lowest() };
    };

    auto surface_area = [](const bbox_t &b) {
        glm::vec3 d = b.max - b.min;
        return 2 * (d.x + d.y + d.z);
    };

    unsigned num_tris = (unsigned)indices.size() / 3;
    bbox_t whole_box;

    std::vector<prim_t> axis_lists[3];
    for (int axis = 0; axis < 3; axis++) {
        axis_lists[axis].reserve(num_tris);
    }

    for (unsigned i = 0; i < indices.size() / 3; i++) {
        unsigned v0 = indices[i * 3],
                 v1 = indices[i * 3 + 1],
                 v2 = indices[i * 3 + 2];

        glm::vec3 _min = glm::min(glm::make_vec3(&attrs[v0 * 8]), glm::make_vec3(&attrs[v1 * 8]), glm::make_vec3(&attrs[v2 * 8])),
                  _max = glm::max(glm::make_vec3(&attrs[v0 * 8]), glm::make_vec3(&attrs[v1 * 8]), glm::make_vec3(&attrs[v2 * 8]));

        whole_box.min = glm::min(whole_box.min, _min);
        whole_box.max = glm::max(whole_box.max, _max);
        
        for (int axis = 0; axis < 3; axis++) {
            axis_lists[axis].push_back({ v0, v1, v2, _min, _max });
        }
    }

    std::vector<bbox_t> right_bounds(num_tris);

    float res_sah = surface_area(whole_box) * num_tris;
    int div_axis = -1;
    unsigned div_index = 0;
    bbox_t res_left_bounds, res_right_bounds;

    for (int axis = 0; axis < 3; axis++) {
        auto &list = axis_lists[axis];

        std::sort(list.begin(), list.end(),
                  [axis](const prim_t &p1, const prim_t &p2) -> bool {
            return p1.bbox_max[axis] < p2.bbox_max[axis];
        });

        bbox_t cur_right_bounds;
        for (unsigned i = (unsigned)list.size() - 1; i > 0; i--) {
            cur_right_bounds.min = glm::min(cur_right_bounds.min, list[i].bbox_min);
            cur_right_bounds.max = glm::max(cur_right_bounds.max, list[i].bbox_max);
            right_bounds[i - 1] = cur_right_bounds;
        }

        bbox_t left_bounds;
        for (unsigned i = 1; i < list.size(); i++) {
            left_bounds.min = glm::min(cur_right_bounds.min, list[i - 1].bbox_min);
            left_bounds.max = glm::max(cur_right_bounds.max, list[i - 1].bbox_max);

            float sah = empty_node_traverse_cost + surface_area(left_bounds) * i + surface_area(right_bounds[i - 1]) * (list.size() - i);
            if (sah < res_sah) {
                res_sah = sah;
                div_axis = axis;
                div_index = i * 3;
                res_left_bounds = left_bounds;
                res_right_bounds = right_bounds[i - 1];
            }
        }
    }

    std::vector<unsigned> res_indices;
    if (div_axis != -1) {
        for (auto &p : axis_lists[div_axis]) {
            res_indices.push_back(p.v0);
            res_indices.push_back(p.v1);
            res_indices.push_back(p.v2);
        }
    } else {
        res_indices = indices;
        res_left_bounds = whole_box;
    }
    
    return{ std::move(res_indices), div_index, { res_left_bounds.min, res_left_bounds.max }, { res_right_bounds.min, res_right_bounds.max } };
}