#pragma once

#include <cassert>
#include <limits>
#include <vector>

#include <glm/common.hpp>

namespace lm {
    enum eMap { PosMap = 1, NormalMap = 2, LightMap = 4, AlbedoMap = 8, LightCacheMap = 16 };

    class LMap {
        int res_;
        std::vector<bool> is_empty_;
        std::vector<glm::vec3> positions_, normals_;
        std::vector<glm::u8vec3> albedo_;
        std::vector<short> heights_; // used for packing
        std::vector<glm::vec3> light_, light_cache_;
        unsigned samples_;

#if defined USE_SW_RENDER
        const float round_off = 0;
#else
        const float round_off = 0.5f;
#endif

        int index(float u) const {
            int x = (int) (u * res_);
            return x < 0 ? 0 : (x > res_ - 1 ? res_ - 1 : x);
        }

        // index with rounding
        int index_r(float u) const {
            if (u < 0) u = 0;
            int x = (int) (u * res_ + round_off);
            return x < 0 ? 0 : (x > res_ - 1 ? res_ - 1 : x);
        }

        inline glm::vec3 lerp(float u, float v, const glm::vec3 *c) const;

    public:
        LMap(int res) : res_(res), samples_(0) {
            assert(res_ % 2 == 0);
            is_empty_.resize((size_t) res_ * res_, true);
            positions_.resize((size_t) res_ * res_);
            normals_.resize((size_t) res_ * res_);
            albedo_.resize((size_t) res_ * res_);
            heights_.resize((size_t) res_);
            light_.resize((size_t) res_ * res_);
            light_cache_.resize((size_t) res_ * res_);
        }

        int res() const { return res_; }

        bool empty(int x, int y) const { return is_empty_[y * res_ + x]; }
        bool empty(float u, float v) const { return empty(index(u), index(1 - v)); }
        void set_empty(int x, int y, bool b) { is_empty_[y * res_ + x] = b; }

        unsigned samples() const { return samples_; }
        void set_samples(unsigned s) { samples_ = s; }
        void incr_samples() { samples_++; }

        const glm::vec3 &pos(int x, int y) const { return positions_[y * res_ + x]; }
        const glm::vec3 &pos(float u, float v) const { return pos(index_r(u), index_r(1 - v)); }
        glm::vec3 pos_lerp(float u, float v) const { return lerp(u, 1 - v, &positions_[0]); }
        void set_pos(int x, int y, const glm::vec3 &p) { positions_[y * res_ + x] = p; }

        const glm::vec3 &normal(int x, int y) const { return normals_[y * res_ + x]; }
        const glm::vec3 &normal(float u, float v) const { return normal(index_r(u), index_r(1 - v)); }
        glm::vec3 normal_lerp(float u, float v) const { return lerp(u, 1 - v, &normals_[0]); }
        void set_normal(int x, int y, const glm::vec3 &n) { normals_[y * res_ + x] = n; }

        const glm::u8vec3 &albedo(int x, int y) const { return albedo_[y * res_ + x]; }
        const glm::u8vec3 &albedo(float u, float v) const { return albedo(index_r(u), index_r(1 - v)); }
        void set_albedo(int x, int y, const glm::u8vec3 &v) { albedo_[y * res_ + x] = v; }

        const glm::vec3 &light(int x, int y) const { return light_[y * res_ + x]; }
        const glm::vec3 &light(float u, float v) const { return light(index_r(u), index_r(1 - v)); }
        glm::vec3 light_lerp(float u, float v) const { return lerp(u, 1 - v, &light_[0]); }
        void set_light(int x, int y, const glm::vec3 &v) { light_[y * res_ + x] = v; }
        void incr_light(int x, int y, const glm::vec3 &new_col) {
            glm::vec3 &cur_col = light_[y * res_ + x];
            cur_col = cur_col + (new_col - cur_col) / (float) samples_;
        }

        const glm::vec3 &light_cache(int x, int y) const { return light_cache_[y * res_ + x]; }
        const glm::vec3 &light_cache(float u, float v) const { return light_cache(index_r(u), index_r(1 - v)); }
        glm::vec3 light_cache_lerp(float u, float v) const { return lerp(u, 1 - v, &light_cache_[0]); }
        void set_light_cache(int x, int y, const glm::vec3 &v) { light_cache_[y * res_ + x] = v; }

        bool FitBlock(int w, int h, int &out_x, int &out_y);

        void ProcessLightCache();

        void FlushSeams(int maps);

        inline void ClearLight();
    };
}

void lm::LMap::ClearLight() {
    std::fill(light_.begin(), light_.end(), glm::vec3());
    samples_ = 0;
}

glm::vec3 lm::LMap::lerp(float u, float v, const glm::vec3 *c) const {
    float x = u * res_, y = v * res_;
    int x0 = index(u), y0 = index(v);
    int x1 = x0 + 1, y1 = y0 + 1;
    x1 = x1 > res_ - 1 ? res_ - 1 : x1;
    y1 = y1 > res_ - 1 ? res_ - 1 : y1;

    float fx0 = x - x0,
          fy0 = y - y0,
          fx1 = 1 - fx0,
          fy1 = 1 - fy0;

    float w1 = fx1 * fy1,
          w2 = fx0 * fy1,
          w3 = fx1 * fy0,
          w4 = fx0 * fy0;

    return w1 * c[y0 * res_ + x0] + w2 * c[y0 * res_ + x1] + w3 * c[y1 * res_ + x0] + w4 * c[y1 * res_ + x1];
}
